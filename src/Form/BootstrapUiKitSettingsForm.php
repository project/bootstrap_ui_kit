<?php
namespace Drupal\bootstrap_ui_kit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class BootstrapUiKitSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'bootstrap_ui_kit.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bootstrap_ui_kit_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $theme = '') {

    $config = $this->config(static::SETTINGS);

    $form['enable_dev_kit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Developer Kit'),
      '#description' => $this->t('Displays example implementations for developers on the UI Kit.'),
      '#default_value' => $config->get('enable_dev_kit'),
    ];

    //
    $form['iconography'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Iconography'),
      '#collapsible' => TRUE,
    ];

    $module_path = \Drupal::service('extension.list.module')->getPath('bootstrap_ui_kit');
    $icons_path = '/vendor/twbs/bootstrap-icons';
    $form['iconography']['icons_source_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icons source folder'),
      '#description' => $this->t('Point this to where all your uncompiled SVG icons are. <br/>ie: <strong>/' . $module_path . $icons_path  . '/icons</strong>'),
      '#default_value' => $config->get('icons_source_folder'),
    ];

    $form['iconography']['icons_sprite_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icons sprite file'),
      '#description' => $this->t('This is the compiled SVG sprite file. <br/>ie: <strong>/' . $module_path . $icons_path  . '/bootstrap-icons.svg' . '</strong>'),
      '#default_value' => $config->get('icons_sprite_file'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('enable_dev_kit', $form_state->getValue('enable_dev_kit'))
      ->set('icons_source_folder', $form_state->getValue('icons_source_folder'))
      ->set('icons_sprite_file', $form_state->getValue('icons_sprite_file'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
